﻿// Requires: RemoteRustCore

using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using Oxide.Core.Plugins;

namespace Oxide.Plugins
{
    [Info("RemoteRust Server Monitor", "AnExiledGod", "0.3.0")]
    [Description("")]
    internal class RemoteRustServerMonitor : CovalencePlugin
    {
        [PluginReference] private Plugin RemoteRust;
        [PluginReference] private Plugin RemoteRustCore;

        private Timer _serverStatusTimer;

        #region Initialization

        private void Loaded()
        {
            _serverStatusTimer = timer.Repeat(30f, 0, ServerStatusHandler);
        }

        private void Unload()
        {
            _serverStatusTimer?.Destroy();
        }

        #endregion
        
        #region Hooks
        
        [HookMethod("GetVersion")]
        private string GetVersionHook()
        {
            return Version.ToString();
        }
        
        #endregion

        // https://umod.org/documentation/games/universal#onpluginloaded
        // https://umod.org/documentation/games/universal#onpluginunloaded
        // https://umod.org/documentation/games/rust#onservercommand
        // https://umod.org/documentation/games/rust#onservershutdown
        // https://umod.org/documentation/games/rust#onrconcommand
        // https://umod.org/documentation/games/rust#onrconconnection
        // https://umod.org/documentation/games/rust#onnewsave
        // https://umod.org/documentation/games/rust#onsaveload
        // https://umod.org/documentation/games/rust#onservermessage
        // https://umod.org/documentation/games/rust#onmessageplayer

        #region Periodic Status Update

        private class ServerStatusData : RemoteRustCore.CoreData
        {
            // Framerate information collected via Performance Singleton
            public string Type = "ServerStatus";
            public int FrameRate;
            public float FrameTime;
            public float FrameRateAverage;
            public float FrameTimeAverage;
            public long SystemMemoryUsage;
            public bool GarbageCollectionTriggered;
            public List<ulong> OnlinePlayers;
            public List<ulong> SleepingPlayers;
        }

        private void ServerStatusHandler()
        {
            Performance.Tick perf = Performance.report;

            ServerStatusData payload = new ServerStatusData()
            {
                FrameRate = perf.frameRate,
                FrameTime = perf.frameTime,
                FrameRateAverage = perf.frameRateAverage,
                FrameTimeAverage = perf.frameTimeAverage,
                SystemMemoryUsage = perf.memoryUsageSystem,
                GarbageCollectionTriggered = perf.gcTriggered,
                OnlinePlayers = RemoteRustCore.Call<List<ulong>>("GetOnlinePlayers"),
                SleepingPlayers = RemoteRustCore.Call<List<ulong>>("GetSleepingPlayers")
            };

            RemoteRust.Call("AddMessageToQueue", JsonConvert.SerializeObject(payload));
        }

        #endregion

        #region OnServerSaved

        private class OnServerSavedData : RemoteRustCore.CoreData
        {
            public string Type = "OnServerSaved";
        }

        private void OnServerSavedHandler()
        {
            OnServerSavedData payload = new OnServerSavedData { };

            RemoteRust.Call("AddMessageToQueue", JsonConvert.SerializeObject(payload));
        }

        void OnServerSave()
        {
            OnServerSavedHandler();
        }

        #endregion
        
        #region OnRconCommand

        private class OnRconCommandData : RemoteRustCore.CoreData
        {
            public string Type = "OnRconCommand";
            public string IPAddress;
            public string Command;
            public string[] Arguments;
        }

        private void OnRconCommandHandler(IPAddress ip, string command, string[] args)
        {
            OnRconCommandData payload = new OnRconCommandData
            {
                IPAddress = ip.ToString(),
                Command = command,
                Arguments = args
            };
            
            RemoteRust.Call("AddMessageToQueue", JsonConvert.SerializeObject(payload));
        }
        
        void OnRconCommand(IPAddress ip, string command, string[] args)
        {
            OnRconCommandHandler(ip, command, args);
        }
        
        #endregion
        
        #region OnServerCommand

        private class OnServerCommandData : RemoteRustCore.CoreData
        {
            public string Type = "OnServerCommand";
            public ulong SteamID;
            public string Command;
            public string Argument;
        }

        private void OnServerCommandHandler(ConsoleSystem.Arg arg)
        {
            OnServerCommandData payload = new OnServerCommandData
            {
                SteamID = arg.Connection?.userid ?? 0,
                Command = arg.cmd.FullName,
                Argument = arg.FullString
            };
            
            RemoteRust.Call("AddMessageToQueue", JsonConvert.SerializeObject(payload));
        }
        
        object OnServerCommand(ConsoleSystem.Arg arg)
        {
            OnServerCommandHandler(arg);
            
            return null;
        }
        
        #endregion
    }
}
